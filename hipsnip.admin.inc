<?php
/**
 * @file
 * Contains the administrative functions of the hipsnip module.
 *
 * This file is included by the core hipsnip module, and includes the
 * settings form.
 */

/**
 * Implements hook_admin_settings_form().
 */
function hipsnip_admin_settings_form() {
  $form = array();

  // Global enabled flag.
  $form['hipsnip_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => variable_get('hipsnip_enabled'),
  );

  // Div Class name.
  $form['hipsnip_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Div class name'),
    '#size' => 25,
    '#required' => TRUE,
    '#default_value' => variable_get('hipsnip_class'),
  );

  // Hipsnip data-width Width.
  $form['hipsnip_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#size' => 3,
    '#maxlength' => 4,
    '#required' => TRUE,
    '#default_value' => variable_get('hipsnip_width', '310'),
  );

  // Hipsnip data-height Height.
  $form['hipsnip_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#size' => 3,
    '#maxlength' => 4,
    '#required' => TRUE,
    '#default_value' => variable_get('hipsnip_height', '250'),
  );

  // Sitename.
  $form['hipsnip_sitename'] = array(
    '#type' => 'textfield',
    '#title' => t('Sitename'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => variable_get('hipsnip_sitename'),
    '#description' => 'Use this format: yoursitename provided by HipSnip Account Manager',
  );

  return system_settings_form($form);
}
