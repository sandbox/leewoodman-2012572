
Module: HipSnip

Description
===========
Provides a block for HipSnip


Installation
============
Copy the 'hipsnip' module directory in to your Drupal: 
/sites/all/modules directory as usual.

Blocks can be placed where you want them on the page using context.
The inline JavaScript will be placed in the footer of the page with a weight 
of 0.


Configuration
====================================================
HipSnip can be configured using the admin settings form:
/admin/config/system/hipsnip

The "sitename" should be set, which will be specific to your application and 
available from your HipSnip account manager.

HipSnip can be disabled by going to /admin/config/system/hipsnip this will 
stop the block appearing without disabling the module - This allows you to 
temporarily turn off HipSnip Block.
