<?php
/**
 * @file
 * HipSnip Container
 */
?>
<div class="<?php print check_plain(variable_get('hipsnip_class'));?>" data-width="<?php print check_plain(variable_get('hipsnip_width'));?>" data-height="<?php print check_plain(variable_get('hipsnip_height'));?>"></div>
